const std = @import("std");

pub const ParseError = error{
    EndOfContent,
};

pub const Error = struct {
    message: []const u8,
    index: usize,
};

pub const Parser = struct {
    allocator: std.mem.Allocator,
    source: []const u8,
    index: usize,
    errors: std.ArrayList(Error),

    pub fn init(allocator: std.mem.Allocator, source: []const u8) Parser {
        var errors = std.ArrayList(Error).init(allocator);

        return Parser{
            .index = 0,
            .source = source,
            .errors = errors,
            .allocator = allocator,
        };
    }

    pub fn deinit(self: *Parser) void {
        self.errors.deinit();
    }

    pub fn peek(self: *Parser) ParseError!u8 {
        if (self.index >= self.source.len) {
            return ParseError.EndOfContent;
        }

        return self.source[self.index];
    }

    pub fn accept(self: *Parser, matcher: fn (*Parser, u8) bool) !bool {
        const tokens = try self.peek();

        return matcher(self, tokens);
    }

    pub fn skipWhile(self: *Parser, matcher: fn (*Parser, u8) bool) !void {
        while (matcher(self, try self.peek())) {
            self.index += 1;
        }
    }

    pub fn acceptWhile(self: *Parser, matcher: fn (*Parser, u8) bool) ![]const u8 {
        var s = std.ArrayList(u8).init(self.allocator);
        defer s.deinit();

        var c = try self.peek();

        while (matcher(self, c)) {
            try s.append(c);
            self.index += 1;
            c = self.peek() catch break;
        }

        return s.toOwnedSlice();
    }
};

pub const Node = union {
    call: struct {
        name: []const u8,
        args: []Node,
    },
    string: []const u8,
};

pub fn parse(parser: *Parser) ![]Node {
    var nodes = std.ArrayList(Node).init(parser.allocator);
    defer nodes.deinit();

    const containsIdent = struct {
        pub fn matches(p: *Parser, s: u8) bool {
            _ = p;
            return std.ascii.isAlNum(s) or s == '_' or s == '.';
        }
    }.matches;

    const skipWs = struct {
        pub fn skip(p: *Parser, s: u8) bool {
            _ = p;
            return std.ascii.isSpace(s);
        }
    }.skip;

    if (try parser.accept(containsIdent)) {
        const ident = try parser.acceptWhile(containsIdent);
        //std.log.debug(
        //"source: {s}, ident: {s}",
        //.{ parser.source[parser.index..], ident },
        //);
        // Try parsing fn call
        try parser.skipWhile(skipWs);

        if (try parser.accept(struct {
            pub fn matches(p: *Parser, s: u8) bool {
                _ = p;
                return s == '(';
            }
        }.matches)) {
            var args = try parser.acceptWhile(struct {
                pub fn matches(p: *Parser, s: u8) bool {
                    _ = p;
                    return s != ')';
                }
            }.matches);

            if (!try parser.accept(struct {
                pub fn matches(p: *Parser, s: u8) bool {
                    _ = p;
                    return s == ')';
                }
            }.matches)) {
                try parser.errors.append(Error{
                    .message = "expected ')' to end function call",
                    .index = parser.index,
                });
            }

            try nodes.append(Node{
                .call = .{
                    .name = ident,
                    .args = &[_]Node{
                        Node{ .string = args },
                    },
                },
            });
        } else {
            std.log.debug("couldn't parse fn call", .{});
        }
    }

    return nodes.toOwnedSlice();
}

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const allocator = arena.allocator();

    const source = try std.fs.cwd().readFileAlloc(
        allocator,
        "main.js",
        4096,
    );

    var parser = Parser.init(allocator, source);
    defer parser.deinit();

    const tree = try parse(&parser);

    for (parser.errors.items) |err| {
        const pos = std.zig.findLineColumn(
            parser.source,
            @intCast(u64, parser.index),
        );
        std.log.err("error at {}:{}: {s}", .{
            pos.line,
            pos.column,
            err.message,
        });
    }

    std.log.debug("nodes: {}", .{tree.len});

    for (tree) |node| {
        std.log.debug("node: {s}", .{node.call.name});
    }
}

test "basic test" {
    try std.testing.expectEqual(10, 3 + 7);
}
